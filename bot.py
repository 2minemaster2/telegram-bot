import logging
import subprocess
import re
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

updater = Updater(token="1081524636:AAEkSDXeklt4hlGRmEJfLNoIh8oOVA8GMAo", use_context=True)

dispatcher = updater.dispatcher

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="I'm running, yeah!")
    context.bot.send_message(chat_id=update.effective_chat.id, text="My commands are:")
    context.bot.send_message(chat_id=update.effective_chat.id, text="/start show this Message")
    context.bot.send_message(chat_id=update.effective_chat.id, text="/yt_download [URL] [File Name] downloads your video and converts it to .aac")
    context.bot.send_message(chat_id=update.effective_chat.id, text="/video_download [URL] [File Name] downloads file and converts it to .mp4")

def yt_download(update, context):
    file_name, url = get_file_name("music", context, update)

    context.bot.send_message(chat_id=update.effective_chat.id, text="The file will be downloaded now!")

    returnValue = subprocess.call([
        "youtube-dl",
        "-x",
        "--audio-format=aac",
        "--audio-quality=0",
        "-o",
        file_name,
        url[1]
    ])

    if returnValue == 0:
        context.bot.send_message(chat_id=update.effective_chat.id, text="The file will be sent now!")
        context.bot.send_audio(chat_id=update.effective_chat.id, audio=open(file_name, 'rb'))
        returnValue = subprocess.call(["rm", file_name])
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text="This URL could not be downloaded, please try again!")

def video_download(update, context):
    file_name, url = get_file_name("video", context, update)

    context.bot.send_message(chat_id=update.effective_chat.id, text="The file will be downloaded now!")

    returnValue = subprocess.call([
        "youtube-dl",
        "--format",
        "mp4",
        "-o",
        file_name,
        url[1]
    ])

    if returnValue == 0:
        context.bot.send_message(chat_id=update.effective_chat.id, text="The file will be sent now!")
        context.bot.send_document(chat_id=update.effective_chat.id, document=open(file_name, 'rb'))
        returnValue = subprocess.call(["rm", file_name])
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text="This URL could not be downloaded, please try again!")

def get_file_name(command_context, context, update):
    user_text = update.message.text
    url = re.split("\s", user_text)

    if command_context == "music":
        command = ["/yt_download"]
        folder = "music/"
        file_extension = ".aac"
    else:
        command = ["/video_download"]
        folder = "video/"
        file_extension = ".mp4"
        default_name = "downloaded_video" + file_extension

    if url == command:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="The url is missing! Please use something like `" + command[0] + " https://www.youtube.com/watch?v=dQw4w9WgXcQ My awesome video`"
        )
        return

    if url[2]:
        file_name = folder + " ".join(url[2:]) + file_extension
    else:
        file_name = folder + default_name

    return (file_name, url)

def unknown(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="Sorry, this command is now known to me :(")

dispatcher.add_handler(CommandHandler('start', start))
dispatcher.add_handler(CommandHandler('yt_download', yt_download))
dispatcher.add_handler(CommandHandler('video_download', video_download))
dispatcher.add_handler(MessageHandler(Filters.text, unknown))

updater.start_polling()
updater.idle()
updater.stop()
