# Alex60015 Bot
Run bot local
```bash
python bot.py
```

Run bot in docker
```bash
$ docker build -t tbot .
$ docker run -d tbot
```

## Commands
### `/start`
returns if the bot is running or not

### `/yt_download`
Downloads the video and converts it to .aac.  
Sends file back to user and deleted locally saved file.  

### `/video_download`
Downloads the video and converts it to .mp4.  
Sends file back to user and deleted locally saved file.  
